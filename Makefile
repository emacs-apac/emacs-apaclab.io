HUGO ?= hugo
FILE_NAME ?= $(shell date "+%B-%Y" | tr '[:upper:]' '[:lower:]').md
ANN_DIR = content/announcements
NOTES_DIR = content/notes
HUGO_VERSION ?= v0.140.2
PODMAN ?= podman
CONTAINER_REGISTRY ?= registry.gitlab.com/emacs-apac/emacs-apac.gitlab.io/hugo

build:
	$(HUGO) --gc --minify

announcement:
	@echo "Creating announcement file in '$(ANN_DIR)'."
	mkdir --parents $(ANN_DIR)
	$(HUGO) new $(ANN_DIR)/$(FILE_NAME) --kind "announcement"

notes:
	@echo "Creating notes file in '$(NOTES_DIR)'"
	mkdir --parents $(NOTES_DIR)
	$(HUGO) new $(NOTES_DIR)/$(FILE_NAME) --kind "notes"

server:
	$(HUGO) server --buildDrafts --buildFuture

build-container:
	$(PODMAN) build -t "${CONTAINER_REGISTRY}:${HUGO_VERSION}" \
	  --build-arg "HUGO_VERSION=${HUGO_VERSION}" .
	$(PODMAN) tag "${CONTAINER_REGISTRY}:${HUGO_VERSION}" \
	  "${CONTAINER_REGISTRY}:latest"

push-container: build-container
	$(PODMAN) push "${CONTAINER_REGISTRY}:${HUGO_VERSION}"
	$(PODMAN) push "${CONTAINER_REGISTRY}:latest"
