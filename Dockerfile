ARG HUGO_VERSION="latest"

FROM ghcr.io/gohugoio/hugo:${HUGO_VERSION}
USER root
# create_announcement.sh uses a date format which is not supported by
# built-in busybox date command.
RUN apk add --no-cache coreutils make
USER hugo:hugo
