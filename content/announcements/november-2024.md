---
title: "CANCELED Announcing Emacs Asia-Pacific (APAC) virtual meetup, Saturday, November 23, 2024"
linktitle: "CANCELED November 2024 virtual meetup"
date: 2024-11-22T01:11:01+05:30
event_date: 2024-11-23T14:00:00+05:30
# previous_event_date: set this when changing event_date
categories:
- Event
---

[Emacs Asia-Pacific (APAC)](https://emacs-apac.gitlab.io) virtual
meetup will not be happening this month. You can join us for the
EmacsConf 2024:

- [EmacsConf 2024 | Online Conference | December 7 and 8, 2024
  (Sat-Sun)](https://emacsconf.org/2024/) from [1900 IST](# "07:00 PM
  Indian Standard Time. UTC+05:30") onwards.
