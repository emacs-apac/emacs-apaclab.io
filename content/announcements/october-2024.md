---
title: "CANCELED Announcing Emacs Asia-Pacific (APAC) virtual meetup, Saturday, October 26, 2024"
linktitle: "CANCELED October 2024 virtual meetup"
date: 2024-10-12T00:02:23Z
event_date: 2024-10-26T14:00:00+00:00
categories:
- Event
---

[Emacs Asia-Pacific (APAC)](https://emacs-apac.gitlab.io) virtual
meetup will not be happening this month as none of the hosts are
available. See you in the next meetup!
