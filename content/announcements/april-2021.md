---
title: "Announcing Emacs Asia-Pacific (APAC) virtual meetup, Saturday, April 24, 2021"
linktitle: "April 2021 virtual meetup"
date: 2021-04-10T00:06:20Z
event_date: 2021-04-24T14:00:00+00:00
categories:
- Event
---

This month's [Emacs Asia-Pacific (APAC)](https://emacs-apac.gitlab.io)
virtual meetup is scheduled for Saturday, April
24, 2021 with Jitsi Meet and `#emacs` on
Freenode IRC. The timing will be [1400 to 1500 IST](# "02:00 PM Indian
Standard Time. UTC+05:30").

### Talks:
- **clj-org-analyzer with Operation Blue Moon data by Shakthi Kannan (~15m)**  
   A demo of clj-org-analyzer and its use for analyzing four years of
  data for the Operation Blue Moon project for managing
  project tasks.  
  - <https://github.com/rksm/clj-org-analyzer/>
  - <https://gitlab.com/shakthimaan/operation-blue-moon>

If you would like to give a demo or talk (maximum 20 minutes) on GNU
Emacs or any variant, please contact `bhavin192` on Freenode with your
talk details:

- Topic
- Description
- Duration
- About Yourself

The Jitsi Meet (video conferencing) URL for the session will be posted
on Freenode IRC channels `#emacs`, `#ilugc` and `#emacsconf`, 30
minutes prior to the meeting, and also on the [ILUGC mailing
list](https://www.freelists.org/list/ilugc) on the day of the
meetup. If you are not subscribed, you can also check the
[archive](https://www.freelists.org/archive/ilugc/).
