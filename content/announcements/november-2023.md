---
title: "CANCELED Announcing Emacs Asia-Pacific (APAC) virtual meetup, Saturday, November 25, 2023"
linktitle: "CANCELED November 2023 virtual meetup"
date: 2023-11-11T00:00:59Z
event_date: 2023-11-25T14:00:00+00:00
categories:
- Event
---

[Emacs Asia-Pacific (APAC)](https://emacs-apac.gitlab.io) virtual
meetup will not be happening this month. You can join us for the
EmacsConf 2023:

- [EmacsConf 2023 | Online Conference | December 2 and 3, 2023
  (Sat-Sun)](https://emacsconf.org/2023/) from [1900 IST](# "07:00 PM
  Indian Standard Time. UTC+05:30") onwards.
