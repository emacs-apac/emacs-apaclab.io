---
title: "CANCELED Announcing Emacs Asia-Pacific (APAC) virtual meetup, Saturday, November 26, 2022"
linktitle: "CANCELED November 2022 virtual meetup"
date: 2022-11-12T00:01:39Z
event_date: 2022-11-26T14:00:00+00:00
categories:
- Event
---

[Emacs Asia-Pacific (APAC)](https://emacs-apac.gitlab.io) virtual
meetup will not happening this month. You can join us for the
EmacsConf 2022:

- [EmacsConf 2022 | Online Conference | December 3 and 4, 2022
  (Sat-Sun)](https://emacsconf.org/2022/) from [1900 IST](# "07:00 PM
  Indian Standard Time. UTC+05:30") onwards.

