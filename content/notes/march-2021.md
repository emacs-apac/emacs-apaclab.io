---
title: "Notes from Emacs Asia-Pacific (APAC) virtual meetup, March 2021"
linktitle: "Notes from March 2021 virtual meetup"
date: 2021-03-29T18:29:28+05:30
categories:
- Event
---

These are the notes from [Emacs Asia-Pacific (APAC) meetup happened on
March 27, 2021](https://emacs-apac.gitlab.io/announcements/march-2021/).

-   R studio / R with Emacs, reproducible research
    -   <https://fosdem.org/2021/schedule/event/open_research_emacs_orgmode/>
    -   <https://m-x-research.github.io/>
-   Literate configuration / Org babel blocks
    -   <https://jherrlin.github.io/posts/emacs-orgmode-source-code-blocks/>
    -   <https://zzamboni.org/post/my-doom-emacs-configuration-with-commentary/>
    -   <http://howardism.org/Technical/Emacs/literate-devops.html>
-   SQL babel blocks (single session for all blocks)
    -   <https://www.orgmode.org/worg/org-contrib/babel/languages/ob-doc-sql.html>
    -   <https://www.emacswiki.org/emacs/SqlMode>
    -   <https://www.youtube.com/watch?v=CGnt_PWoM5Y>
-   Doom Emacs on Windows
    -   <https://www.reddit.com/r/emacs/comments/m7pe6c/wsl2emacsvcxsrv_open_everything_with_native/>
    -   <https://www.youtube.com/watch?v=3qDoHd-6NOQ>
-   Personal wiki based on Org
    -   Org Rome, Org Deft
    -   Using grep, ag, silversearcher.
    -   [yantar92] using # appended words as keywords, use org-ql, and then Helm to perform actions on those Org entries.
-   Committing the Org files to source control
    -   Using Org habit to review and commit them daily.
    -   [yantar92] auto commit function to be ran in git status buffer: <https://github.com/yantar92/emacs-config/blob/master/config.org#auto-commit-trivial-changes-in-org-files>
-   Artist mode
    -   <http://ditaa.sourceforge.net/>
    -   <http://www.graphviz.org/>
    -   <https://correl.phoenixinquis.net/2015/07/12/git-graphs.html>
-   Org habits / clocking / time graphs
    -   <https://github.com/rksm/clj-org-analyzer>
    -   <https://samplesize.one/blog/posts/my_year_in_data/>
    -   <https://activitywatch.net/>
-   Logging state changes, notes etc

    ```lisp
    (setq org-log-state-notes-insert-after-drawers t)
    ```
-   Navigating to buffers, window configurations
    -   <https://github.com/pashinin/workgroups2>
    -   <https://github.com/Bad-ptr/persp-mode.el>
    -   <https://github.com/alphapapa/burly.el>
    -   Helm mini: <https://github.com/emacs-helm/helm/wiki>
    -   Using tabs as workspaces.
    -   [EvilRocks]

        ```lisp
        ;; Show projectile files in helm mini
        (setq helm-mini-default-sources
              '(helm-source-buffers-list
        	helm-source-projectile-dired-files-list
        	helm-source-projectile-files-list
        	helm-source-recentf
        	helm-source-buffer-not-found))
        ```
-   Configuration
    -   <https://github.com/seshaljain/.emacs.d>
    -   <https://github.com/seshaljain/.doom.d>

Credits: A, archgaelix, bhavin192, EvilRocks, mbuf, meain, yantar92.
