---
title: "Notes from Emacs Asia-Pacific (APAC) virtual meetup, April 2021"
linktitle: "Notes from April 2021 virtual meetup"
date: 2021-05-28T19:45:41+05:30
categories:
- Event
---

These are the notes from [Emacs Asia-Pacific (APAC) meetup happened on
April 24, 2021](https://emacs-apac.gitlab.io/announcements/april-2021/).

-   Goals and working weekly, yearly
    -   yantar92: using SMART for goals.
    -   mbuf: OBM uses top down approach.
    -   mbuf: what you want to do, when you wan't to do.
-   Org brain / Org roam
    -   <https://emacsconf.org/2020/talks/12/>
    -   <https://emacsconf.org/2020/talks/16/>
    -   <https://emacsconf.org/2020/talks/18/>
-   Org Crypt rotating key
    -   yantar92: one can write a function, which traverses all
        headings and re-encrypts them with new key.

Credits: bhavin192, mbuf, yantar92.
