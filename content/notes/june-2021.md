---
title: "Notes from Emacs Asia-Pacific (APAC) virtual meetup, June 2021"
linktitle: "Notes from June 2021 virtual meetup"
date: 2021-06-27T21:03:02+05:30
categories:
- Event
---

These are the notes from [Emacs Asia-Pacific (APAC) meetup happened on
June 26, 2021](https://emacs-apac.gitlab.io/announcements/june-2021/).

-   Showing trailing whitespaces in the current buffer
    -   [irek] I keep whitespace-mode on in some modes.
    -   show-trailing-whitespace: <https://stackoverflow.com/q/11700934/6202405>
-   rainbow-delimiters: different color theme
    -   <https://github.com/Fanael/rainbow-delimiters>
    -   <https://twitter.com/susam/status/1408066764705984514>
-   Browsing DevDocs (devdocs.io) in eww
    -   <https://github.com/blahgeek/emacs-devdocs-browser>
    -   <https://github.com/sunaku/dasht>
-   [ ] ox-hugo publishing: marking posts as draft
    -   [archgaelix] want to mark the TODO entries as draft posts.
-   Building Emacs with correct flags
    -   [bhavin192] I build the RPM from source <https://geeksocket.in/posts/emacs-pretest-rpm/#cloning-the-git-repository>
    -   [meain] for mac users building Emacs, try <https://github.com/railwaycat/homebrew-emacsmacport> It seems to be much faster generally.
-   Hiding the content of a selected region
    -   [bhavin192] `C-x n s` (in Org mode to narrow to the tree).
    -   [archgaelix] I use hide-show for code blocks.
    -   [meain] <https://github.com/magnars/fold-this.el>
-   [ ] Issues with straight.el on macOS
    -   [meain] I'm not able to fetch things, get SSL related errors.

        ```
        fatal: unable to access 'https://gitlab.com/ideasman42/emacs-mode-line-idle.git/': SSL certificate problem: unable to get local issuer certificate
        ```
-   Using shell inside Emacs
    -   [bhavin192] the `TERM` variable can be set to blank when running inside an Emacs shell, with that most of the CLI tools will not print unsupported escape sequences.
    -   [irek] I have set `PAGER` to cat and `EDITOR` to ed.
    -   [bhavin192] I use emacsclient when using `shell` <https://gitlab.com/bhavin192/dotfiles/-/blob/40ba2d7ac8cc4fe41b0593898e17f924323b4cdf/.bashrc#L116-122>
    -   [mohan43u] `ansi-xterm` can be used as well.
    -   [meain] speed comparison between vterm and ansi-term. <https://github.com/akermu/emacs-libvterm>
-   Elfeed utility functions
    -   <https://github.com/skeeto/elfeed>
    -   [meain] utility functions to do extra filtering based on tags and titles of the feed entries.
    -   <https://github.com/meain/dotfiles/blob/aa89b7be6fbd89ffabb5ccd28ab36df68593b400/emacs/.config/emacs/init.el#L1596>

Credits: archgaelix, bhavin192, irek, meain, mohan43u, Susam, Yuchen.
